% Loads block re-ordered matrix A and corresponding RHS b
%
% Requires that level, domain_type, stretch_a, stretch_b set already;

filename = ['../jacs_and_rhs/RESLT_prec5_n_v_cycle0_n_smoother_iterations0_nel_1d',num2str(2^level),'_horizontal_stretch_factor',stretch_a,'_top_right_vertical_stretch_factor',stretch_b,'_',domain_type,'_CG/'];
B = load([filename,'global_jac.dat']);
row=B(:,1); 
col=B(:,2); 
val=B(:,3);
n=max(row)+1;
nn=size(row); nnz=nn(1);
bs=n(1)/4;
for i=1:nnz
   dt=0; nb=0; 
   dt=mod(row(i),4);     %  DOF type
   nb=floor(row(i)/4);   %  position withing the block
   row(i)=dt*bs+nb;      %  new row index
   dt=0; nb=0;
   dt=mod(col(i),4);     %  DOF type
   nb=floor(col(i)/4);   %  position within the block
   col(i)=dt*bs+nb;      %  new column index
end
row=row+1; col=col+1;
A=sparse(row,col,val); 
clear B; clear row; clear col; clear val;

%  matrix blocking
A11=A(1:bs,1:bs); A12=A(1:bs,bs+1:2*bs); 
A13=A(1:bs,2*bs+1:3*bs); A14=A(1:bs,3*bs+1:n);
A21=A(bs+1:2*bs,1:bs); A22=A(bs+1:2*bs,bs+1:2*bs);
A23=A(bs+1:2*bs,2*bs+1:3*bs); A24=A(bs+1:2*bs,3*bs+1:n);
A31=A(2*bs+1:3*bs,1:bs); A32=A(2*bs+1:3*bs,bs+1:2*bs);
A33=A(2*bs+1:3*bs,2*bs+1:3*bs); A34=A(2*bs+1:3*bs,3*bs+1:n);
A41=A(3*bs+1:n,1:bs); A42=A(3*bs+1:n,bs+1:2*bs);
A43=A(3*bs+1:n,2*bs+1:3*bs); A44=A(3*bs+1:n,3*bs+1:n);

A=[A11,A12,A13,A14;A21,A22,A23,A24;A31,A32,A33,A34;A41,A42,A43,A44];

O=sparse(bs,bs);

% RHS
b = load([filename,'/global_res.dat']);
b = b(:,2);
N = size(A,1);
per = [1:4:N,2:4:N,3:4:N,4:4:N];
b = b(per);
b(N/4+1:end) = 0;
return