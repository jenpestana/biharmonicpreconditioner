% Runs Matlab CG on all preconditioners and levels. If MI20 is
% installed set use_hsl to 1 and change the MI20 path on line 27
%
%
% Options
use_hsl = 0;                    % Use MI20 code
save_data = 0;                  % Saves to .mat file
make_tables = 1;                % Makes tables and saves in ./Tex
compute_errors = 0;             % Compute A-norm of error at termination
test_type = 1;                  % 1: square, 2: stretched, 3: distorted, 
                                % 4: curved

levset = 2:7;                   % Grids: mesh width = 2^levset(i)
numlev = length(levset);        % Number of grids

% Defines preconditioners; needed to determine number of pre.
prelist = {'Unpreconditioned','AMG','$\pc_J$','$\pcbd$','$\pcbbd$',...
    '$\widetilde\pc_{BBD}^{[LU]}$','$\widetilde\pc_{BBD}^{[AMG]}$'};
numpre = length(prelist);       % Number of pre

Nset = zeros(numlev,1);         % Stores dimension of A
itmat = zeros(numlev,numpre);   % Stores iterations
rvmat = zeros(numlev,numpre);   % Stores CG residual at termination
ermat = zeros(numlev,numpre);   % Stores A norm of error

if use_hsl == 1
    addpath('/home/mbbssjp5/Code/hsl_mi20-1.6.0/matlab');
end

for level = levset
    display(level)
    
    % Get A
    switch test_type 
        case 1
            stretch_a = '1.0';
            stretch_b = '1.0';
            domain_type = 'polygonal';
        case 2
            stretch_a = '2.5';
            stretch_b = '1.0';
            domain_type = 'polygonal';
        case 3
            stretch_a = '1.0';
            stretch_b = '1.5';
            domain_type = 'polygonal';
        case 4
            stretch_a = '1.0';
            stretch_b = '1.0';
            domain_type = 'curved';
    end
    bih_mat_rhs;
    n = size(A11,1);                    % Dimension of each block
    N = 4*n;
    Nset(level-1) = N;
    
    %Set up algebraic preconditioners
    PJ = blkdiag(A11,A22,A33,A44);      % Block Jacobi
    PBD = blkdiag(A(1:3*n,1:3*n),A44);  % BD
    PBBD = PBD;                         % BBD
    PBBD(n+1:2*n,2*n+1:3*n) = 0;
    PBBD(2*n+1:3*n,n+1:2*n) = 0;
    
    % Lumping/Diag approx
    L22 = spdiags(sum(A22)',0,n,n);
    L33 = spdiags(sum(A33)',0,n,n);
    D44 = spdiags(diag(A44),0,n,n);
    
    PBBDL = PBBD;                       % PBBDLU
    PBBDL(n+1:N,n+1:N) = blkdiag(L22,L33,D44);
    save applyAMGMat A11 L22 L33 D44 A12 A13
    clear L22 L33 A11 A22 A33 A44 D44 A12 A13
    
    % Symmetrize matrices and ensure sparse
    A = (A+A')/2;
    PJ = (PJ+PJ')/2; PJ = sparse(PJ);
    PBD = (PBD+PBD')/2; PBD = sparse(PBD);
    PBBD = (PBBD + PBBD')/2;  PBBD = sparse(PBBD);
    PBBDL = (PBBDL+PBBDL')/2; PBBDL = sparse(PBBDL);
    
    if use_hsl == 1
        control = hsl_mi20_control;
        control.c_fail = 2;
        control.v_iterations = 2;
        inform = hsl_mi20_setup(A,control);
    end
    
    % CG parameters
    tol = 1e-6;
    maxit = N;
    xex = A\b; xan = sqrt(xex'*A*xex);
    
    % PCG
    [xA,~,rA,iA] = pcg(A,b,tol,N);
    [xJ,~,rJ,iJ] = pcg(A,b,tol,maxit,PJ);
    [xBBD,~,rBBD,iBBD] = pcg(A,b,tol,maxit,PBBD);
    [xBD,~,rBD,iBD] = pcg(A,b,tol,maxit,PBD);
    [xBBDL,~,rBBDL,iBBDL] = pcg(A,b,tol,maxit,PBBDL);
    if use_hsl == 1
        [xAMG,~,rAMG,iAMG] = pcg(A,b,tol,N,'hsl_mi20_precondition'); % AMG on full matrix
        [xBBDAMG,~,rBBDAMG,iBBDAMG]=pcg(A,b,tol,maxit,@(x)applyAMGpre(x,control));
        hsl_mi20_finalize;
    else
        xAMG = NaN; rAMG = NaN; iAMG = NaN;
        xBBDAMG = NaN; rBBDAMG = NaN; iBBDAMG = NaN;
    end
    
    % Compute errors
    if compute_errors == 1
        eA = xA - xex;
        neA = sqrt(eA'*A*eA)/xan;
        eJ = xJ - xex;
        neJ = sqrt(eJ'*A*eJ)/xan;
        eBD = xBD - xex;
        neBD = sqrt(eBD'*A*eBD)/xan;
        eBBD = xBBD - xex;
        neBBD = sqrt(eBBD'*A*eBBD)/xan;
        eBBDL = xBBDL - xex;
        neBBDL = sqrt(eBBDL'*A*eBBDL)/xan;
        eAMG = xAMG - xex;
        neAMG = sqrt(eAMG'*A*eAMG)/xan;
        eBBDAMG = xBBDAMG - xex;
        neBBDAMG = sqrt(eBBDAMG'*A*eBBDAMG)/xan;
        ermat(level-1,:) = [neA neAMG neJ neBD neBBD neBBDL neBBDAMG];
    end
    
    itmat(level-1,:) = [iA iAMG iJ iBD iBBD iBBDL iBBDAMG];
    rvmat(level-1,:) = [rA rAMG rJ rBD rBBD rBBDL rBBDAMG];
    
    if save_data == 1 && compute_errors == 0
        save CGMAT itmat rvmat
    elseif save_data == 1 && compute_errors == 1
        save CGMAT itmat rvmat ermat
    end
end

delete applyAMGMat.mat

if make_tables == 1
    % open files
    fid = fopen('CGIter.tex','w+');
    
    % set up first two rows
    fprintf(fid,'Elements ');
    for i = 1:numlev
        fprintf(fid,'& $%i \\times %i$ ',2^levset(i),2^levset(i));
    end
    fprintf(fid,'\\\\\n');
    fprintf(fid,' $N$ ');
    for i = 1:numlev
        fprintf(fid,'& %i ',Nset(i));
    end
    fprintf(fid,'\\\\\n\\hline\n');
    
    % Print PCG iters
    for k = 1:numpre
        fprintf(fid,'%s ',prelist{k});
        for i = 1:numlev
            fprintf(fid,'& %i ', itmat(i,k));
        end
        fprintf(fid,'\\\\\n');
    end
    fclose all;
end