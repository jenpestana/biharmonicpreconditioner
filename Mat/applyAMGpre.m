function [ y ] = applyAMGpre( x,control )

% Applies the approximate BBD preconditioner with lumping and MI20 AMG

N = length(x);
n = N/4;

load applyAMGMat;

x1 = x(1:n);
x2 = x(n+1:2*n);
x3 = x(2*n+1:3*n);
x4 = x(3*n+1:4*n);

x1 = x1 - A12*(L22\x2) - A13*(L33\x3); % Apply U

S11 = A11 - A12*(L22\(A12')) - A13*(L33\(A13'));
S11 = (S11 + S11')/2;
inform = hsl_mi20_setup(S11,control);

y1 = hsl_mi20_precondition(x1);
y2 = L22\(x2 - A12'*y1);
y3 = L33\(x3 - A13'*y1);
y4 = D44\x4;

y = [y1;y2;y3;y4];