# README #

This repository contains Matlab code for computing tables in Manchester MIMS technical report 2015.23: *Efficient block preconditioning for a C1 finite element discretisation of the Dirichlet biharmonic problem* by Jennifer Pestana, Richard Muddle, Matthias Heil, Françoise Tisseur and Milan Mihajlović. 

### How do I get set up? ###

* The primary files are located in ./Mat 
    - *PreconditionerCG* runs preconditioned CG with all preconditioners; 
    - *PreconditionerEigs* computes the extreme eigenvalues for each preconditioned matrix; 
    - *PreconditionerElementInfo* computes quantities in Section 3 and lumping/diagonal approximation bounds in Section 4 
    - *geteffectiveconditionnumber* computes the effective condition number. 
* All other files in ./Mat are dependencies
* Files in ./jacs_and_rhs contain the matrices and right-hand sides (computed using OOMPH-LIB) for the biharmonic problem discretised by Hermite bicubic elements
* Note that to use the AMG solver it is necessary to download the [HSL code MI20](http://www.hsl.rl.ac.uk/catalogue/hsl_mi20.html) with the Matlab interface, and update the path to MI20 in PreconditionerCG and PreconditionerEigs