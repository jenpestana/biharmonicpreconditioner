% Computes delta_min and eigenvalue bounds from section 3 and lumping
% and diagonal approximation bounds from Section 4.

lev = 2:6;
numlev = length(lev);
Nset = zeros(numlev,1);             % Dimension of large matrix
stretch_vec = 1:0.5:2.5;            % List of stretch factors
numstretch = length(stretch_vec);

delta_eigs = zeros(numstretch,3,numlev);    % largest and smallest eigenvalue of (large)
                                            % preconditioned matrix and delta_min
elementeigs = zeros(numstretch,1);          % Matrix of theta values in (3.14)
lumpedmat = zeros(numstretch,6);            % Stores lumped/diag element matrix eigs

make_tables = 1;

for j = 1:numstretch                % Loop over stretch factors
    stretch_fac = stretch_vec(j);   % Current stretch factor
    for levelind = lev              % Loop over levels
        kk = 2^levelind;
        level = log2(kk);
        
        % Get large matrix
        switch stretch_fac
            case 1
                stretch_a = '1.0';
            case 1.5
                stretch_a = '1.5';
            case 2
                stretch_a = '2.0';
            case 2.5
                stretch_a = '2.5';
        end
        stretch_b = '1.0';
        domain_type = 'polygonal';
        bih_mat_rhs;
        AA = sparse(A);
        Nset(levelind-1) = size(AA,1);
        
        % Get large block diagonal preconditioner
        PP = AA; n = size(A,1)/4;
        PP(1:3*n,3*n+1:4*n) = 0;
        PP(3*n+1:4*n,1:3*n) = 0;
        PP = sparse(PP);
        AA = (AA + AA')/2; PP = (PP + PP')/2;
        
        delta_eigs(j,1,levelind-1) = eigs(AA,PP,1,'sa');
        delta_eigs(j,2,levelind-1) = eigs(AA,PP,1,'la');
        
        
        %%% Block diagonal element preconditioner
        switch stretch_fac
            case 1
                element_matrix_a1_0_b1;
            case 1.5
                element_matrix_a1_5_b1;
            case 2
                element_matrix_a2_0_b1;
            case 2.5
                element_matrix_a2_5_b1;
        end
        p = [1 2 4 3 5 6 8 7 9 10 12 11 13 14 16 15]; % For consistency with other codes
        A = A(p,p);
        P = A;
        n = 4;
        P(1:3*n,3*n+1:4*n) = 0; P(3*n+1:4*n,1:3*n) = 0;
        
        % Set up block diagonal matrices
        Pe = kron(speye(kk^2),P);
        Ae = kron(speye(kk^2),A);
        
        % Find ranges and nullspaces:
        % AE range and null
        ra = rank(full(A)); za = 4*n-ra;
        [~,~,V] = svd(full(A));
        RA = V(:,1:ra);
        ZA = V(:,ra+1:4*n);
        
        % PE range and null
        rp = rank(full(P)); zp = 4*n-rp;
        [~,~,V] = svd(full(P));
        RP = V(:,1:rp);
        ZP = V(:,rp+1:4*n);
        
        % Null(A) not in null(P)
        T = ZA - ZP*ZP'*ZA;
        [U,~,~] = svd(T);
        ZN = U(:,1:za-zp);
        
        % Connectiviy matrix
        L = conn(kk);
        per =  [1:4:16 2:4:16 3:4:16 4:4:16];
        [~,nl] = size(L); Q = eye(16); Q = Q(:,per);
        Q = kron(speye(kk^2),Q);
        Ltemp = Q*L;
        ind = [1:4:nl,2:4:nl,3:4:nl,4:4:nl];
        LL = Ltemp(:,ind);
        
        % Prolongate x and then find components in each subspace
        PZP = kron(speye(kk^2),ZP*ZP');
        PZN = kron(speye(kk^2),ZN*ZN');
        PRA = kron(speye(kk^2),RA*RA');
        
        % Smallest eigenvalue
        [vmin,lmin] = eigs(LL'*kron(speye(kk^2),(RA*RA')*P*(RA*RA'))*LL,LL'*kron(speye(kk^2),(ZN*ZN')*P*(ZN*ZN'))*LL,1,'sm');
        delta_eigs(j,3,levelind-1) = lmin;
    end
    
    % Eigenvalues of lumped element matrices
    Ae22 = A(5:8,5:8);
    Ae33 = A(9:12,9:12);
    Ae44 = A(13:16,13:16);
    Le22 = diag(sum(abs(Ae22),2));
    Le33 = diag(sum(abs(Ae33),2));
    De44 = diag(diag(Ae44));
    e22 = eig(Ae22,Le22);
    e33 = eig(Ae33,Le33);
    e44 = eig(Ae44,De44);
    lumpedmat(j,:) = [min(e22),max(e22),min(e33),max(e33),min(e44),max(e44)];
    Ar = RA'*A*RA; Ar = (Ar + Ar')/2;
    Pr = RA'*P*RA; Pr = (Pr + Pr')/2;
    elementeigs(j) = min(eig(Ar,Pr));
end


if make_tables == 1
    fid = fopen('ElementEigStretch.tex','w+');
    
    % set up first two rows
    
    fprintf(fid,'& ');
    fprintf(fid,'Elements ');
    for i = 1:numlev
        fprintf(fid,'& $%i \\times %i$ ',2^lev(i),2^lev(i));
    end
    fprintf(fid,'\\\\\n');
    
    fprintf(fid,'& ');
    fprintf(fid,' $N$ ');
    for i = 1:numlev
        fprintf(fid,'& %i ',Nset(i));
    end
    fprintf(fid,'\\\\\n\\hline\n');
    
    for j = 1:numstretch
        fprintf(fid,'\\multirow{3}{*}{$a=%3.2g$} & ',stretch_vec(j));
        
        fprintf(fid,'$ \\lambda_{\\min}$ ');
        for i = 1:numlev
            fprintf(fid,'& %5.2g ', delta_eigs(j,1,i));
        end
        fprintf(fid,'\\\\\n');
        
        fprintf(fid,'& $\\lambda_{\\max}$ ');
        for i = 1:numlev
            fprintf(fid,'& %5.3g ', delta_eigs(j,2,i));
        end
        fprintf(fid,'\\\\\n');
        
        fprintf(fid,'& $\\delta_{\\min}$ ');
        for i = 1:numlev
            fprintf(fid,'& %5.2g ',delta_eigs(j,3,i));
        end
        fprintf(fid,'\\\\\n\\hline\n');
    end
    
    fclose(fid);
    
    fid = fopen('ElementBoundStretch.tex','w+');
    fprintf(fid,'$a$ ');
    for j = 1:numstretch
        fprintf(fid,'& %3.2g ',stretch_vec(j));
    end
    fprintf(fid,'\\\\\n\\hline\n');
    
    % delta^ast
    fprintf(fid,'$\\delta^\\ast$ ');
    for j = 1:numstretch
        fprintf(fid,'& %5.3g ',delta_eigs(j,3,end));
    end
    fprintf(fid,'\\\\\n');
    
    % zeta
    zeta = 2*(1+1./squeeze(delta_eigs(:,3,end)));
    fprintf(fid,'$\\zeta$ ');
    for j = 1:numstretch
        fprintf(fid,'& %5.2g ',zeta(j));
    end
    fprintf(fid,'\\\\\n');
    
    % theta
    fprintf(fid,'$\\theta$ ');
    for j = 1:numstretch
        fprintf(fid,'& %5.2g ',elementeigs(j));
    end
    fprintf(fid,'\\\\\n');
    
    % theta/zeta
    fprintf(fid,'$\\theta/\\zeta$ ');
    for j = 1:numstretch
        fprintf(fid,'& %7.5g ',elementeigs(j)/zeta(j));
    end
    fprintf(fid,'\\\\\n');
    fclose(fid);
end
