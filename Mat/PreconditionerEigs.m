% Gets smallest and largest eigenvalues for all preconditioned matrices. 
% If MI20 is installed set use_hsl to 1 and change the MI20 path on line 23

% Options
use_hsl = 1;                        % Use MI20 code
save_data = 0;                      % Saves to .mat file
make_tables = 1;                    % Makes tables and saves in ./Tex
test_type = 1;                      % 1: square, 2: stretched, 3: distorted, 
                                    % 4: curved
levset = 2:6;                       % Grids: mesh width = 2^levset(i)
numlev = length(levset);            % Number of levels
Nset = zeros(numlev,1);             % Stores dimension of A
Brank = zeros(numlev,1);            % Stores rank of B
A23rank = zeros(numlev,1);          % Stores rank of A23
numtype = 7;                        % Number of preconditioners

eigmat = zeros(numlev,2*numtype);   % Store eigenvalues
fulleiglev = 6;                     % Level at which switch from eig to eigs
eigopts.maxit = 1000; 
eigopts.p = 100;

if use_hsl == 1 % Setup for MI20
    addpath('/home/mbbssjp5/Code/hsl_mi20-1.6.0/matlab');
    control = hsl_mi20_control;
    control.c_fail = 2;
    control.v_iterations = 2;
end

for level = levset;
    display(level)
    % Get A
    switch test_type 
        case 1
            stretch_a = '1.0';
            stretch_b = '1.0';
            domain_type = 'polygonal';
        case 2
            stretch_a = '2.5';
            stretch_b = '1.0';
            domain_type = 'polygonal';
        case 3
            stretch_a = '1.0';
            stretch_b = '1.5';
            domain_type = 'polygonal';
        case 4
            stretch_a = '1.0';
            stretch_b = '1.0';
            domain_type = 'curved';
    end
    bih_mat_rhs;
    n = size(A11,1);                    % Size of each block
    Nset(level-1) = 4*n;                  % Store dimension of A
    A23rank(level-1) = rank(full(A23));   % Compute rank of A23
    B = [A41 A42 A43];                  % Form B
    Brank(level-1) = rank(full(B));       % Compute rank of B
    clear B
    
    % A Eigs
    A = (A+A')/2;
    if level < fulleiglev
        e = eig(full(A));
        eminA = min(e);
        emaxA = max(e);
    else
        eminA = eigs(A,1,'sa',eigopts);
        emaxA = eigs(A,1,'la',eigopts);
    end
    
    % Block Jacobi Eigs
    PJ = blkdiag(A11,A22,A33,A44);
    PJ = (PJ+PJ')/2;
    if level < fulleiglev
        e = eig(full(A),full(PJ));
        eminJ = min(e);
        emaxJ = max(e);
    else
        eminJ = eigs(A,PJ,1,'sa',eigopts);
        emaxJ = eigs(A,PJ,1,'la',eigopts);
    end
    clear PJ;
    
    % MI20 AMG Eigs
    if use_hsl == 1
        if level < fulleiglev
            PAMG = sparse(4*n,4*n);
            inform = hsl_mi20_setup(A,control);
            for j = 1:4*n
                PAMG(:,j) = hsl_mi20_precondition(A(:,j));
            end
            e = eig(full(PAMG));
            eminAMG = min(e);
            emaxAMG = max(e);
        else
             eminAMG = NaN; emaxAMG = NaN;
        end
        clear PAMG;
    else
        eminAMG = NaN; emaxAMG = NaN;
    end
        
    % PBD Eigs
    PBD = blkdiag(A(1:3*n,1:3*n),A44);
    PBD = (PBD+PBD')/2;
    if level < fulleiglev
        e = eig(full(A),full(PBD));
        eminBD = min(e);
        emaxBD = max(e);
    else
        eminBD = eigs(A,PBD,1,'sa',eigopts);
        emaxBD = eigs(A,PBD,1,'la',eigopts);
    end
    
    %PBBD Eigs
    PBBD = PBD;
    PBBD(n+1:2*n,2*n+1:3*n) = sparse(n,n);
    PBBD(2*n+1:3*n,n+1:2*n) = sparse(n,n);
    PBBD = (PBBD + PBBD')/2;
    chol(PBBD);
    if level < fulleiglev
        e = eig(full(A),full(PBBD));
        eminBBD = min(e);
        emaxBBD = max(e);
    else
        eminBBD = eigs(A,PBBD,1,'sa',eigopts);
        emaxBBD = eigs(A,PBBD,1,'la',eigopts);
    end
    clear PBD
    
    % Lumped PBBD Eigs (without AMG approx.)
    L22 = spdiags(sum(A22)',0,n,n);
    L33 = spdiags(sum(A33)',0,n,n);
    D44 = spdiags(diag(A44),0,n,n);
    PBBDL = PBBD; PBBDL(n+1:4*n,n+1:4*n) = blkdiag(L22,L33,D44);
    PBBDL = (PBBDL+PBBDL')/2;
    if level < fulleiglev
        e = eig(full(A),full(PBBDL));
        eminBBDL = min(e);
        emaxBBDL = max(e);
    else
        eminBBDL = eigs(A,PBBDL,1,'sa',eigopts);
        emaxBBDL = eigs(A,PBBDL,1,'la',eigopts);
    end
    
    save applyAMGMat A11 L22 L33 D44 A12 A13
    clear PBBD L22 L33 A11 A22 A33 A44 D44 A12 A13 PBBDL
    
    % MI20 AMG Lumped PBBD Eigs
    if use_hsl == 1
        PBBDAMG = sparse(4*n,4*n);
        if level < fulleiglev
            for j = 1:4*n
                PBBDAMG(:,j) = applyAMGpre(A(:,j),control);
            end
            e = eig(full(PBBDAMG));
            eminPBBDAMG = min(e);
            emaxPBBDAMG = max(e);
        else
             eminPBBDAMG = NaN; emaxPBBDAMG = NaN;
        end
    else
        eminPBBDAMG = NaN; emaxPBBDAMG = NaN;
    end
    
    % Store eigs
    eigmat(level-1,1:6) = [eminA emaxA eminAMG emaxAMG eminJ emaxJ];
    eigmat(level-1,7:12) = [eminBBD emaxBBD eminBD emaxBD eminBBDL emaxBBDL];
    eigmat(level-1,13:14) = [eminPBBDAMG emaxPBBDAMG];
    
    if save_data == 1
        save EIGMAT eigmat Brank A23rank Nset
    end
end

delete applyAMGMat.mat

% Tables
if make_tables == 1
    fidA = fopen('EigA.tex','w+');
    fidBD = fopen('EigBD.tex','w+');
    fidBBD = fopen('EigBBD.tex','w+');
    fidApp = fopen('EigApp.tex','w+');
    fidOth = fopen('EigOth.tex','w+');
    
    fileset = {fidA,fidBD,fidBBD,fidApp,fidOth};
    
    % set up first two rows for all tables
    for k = 1:length(fileset)
        fid = fileset{k};
        
        if k > 3
            fprintf(fid,'& ');
        end
        fprintf(fid,'Elements ');
        for i = 1:numlev
            fprintf(fid,'& $%i \\times %i$ ',2^levset(i),2^levset(i));
        end
        fprintf(fid,'\\\\\n');
        
        if k > 3
            fprintf(fid,'& ');
        end
        fprintf(fid,' $N$ ');
        for i = 1:numlev
            fprintf(fid,'& %i ',Nset(i));
        end
        fprintf(fid,'\\\\\n\\hline\n');
    end
    
    % fid A
    fprintf(fidA,'$\\lambda_{\\min}$ ');
    for i = 1:numlev
        fprintf(fidA,'& %6.4g ', eigmat(i,1));
    end
    fprintf(fidA,'\\\\\n');
    
    fprintf(fidA,'$\\lambda_{\\max}$ ');
    for i = 1:numlev
        fprintf(fidA,'& %i ', round(eigmat(i,2)));
    end
    fprintf(fidA,'\\\\\n');
    
    fprintf(fidA,'$\\kappa$ ');
    for i = 1:numlev
        fprintf(fidA,'& %i ', round(eigmat(i,2)./eigmat(i,1)));
    end
    fprintf(fidA,'\\\\\n');
    
    % fid BD
    fprintf(fidBD,'$\\lambda_{\\min}$ ');
    for i = 1:numlev
        fprintf(fidBD,'& %4.2g ', eigmat(i,9));
    end
    fprintf(fidBD,'\\\\\n');
    
    fprintf(fidBD,'$\\lambda_{\\max}$ ');
    for i = 1:numlev
        fprintf(fidBD,'& %4.3g ', eigmat(i,10));
    end
    fprintf(fidBD,'\\\\\n');
    
    fprintf(fidBD,'$\\rank(B)$ ');
    for i = 1:numlev
        fprintf(fidBD,'& %i ', Brank(i));
    end
    fprintf(fidBD,'\\\\\n');
    
    % fid BBD
    fprintf(fidBBD,'$\\lambda_{\\min}$ ');
    for i = 1:numlev
        fprintf(fidBBD,'& %4.2g ', eigmat(i,7));
    end
    fprintf(fidBBD,'\\\\\n');
    
    fprintf(fidBBD,'$\\lambda_{\\max}$ ');
    for i = 1:numlev
        fprintf(fidBBD,'& %4.3g ', eigmat(i,8));
    end
    fprintf(fidBBD,'\\\\\n');
    
    fprintf(fidBBD,'$\\rank(A_{23})$ ');
    for i = 1:numlev
        fprintf(fidBBD,'& %i ', A23rank(i));
    end
    fprintf(fidBBD,'\\\\\n');
    
    % fid APPROX
    fprintf(fidApp,'$\\multirow{2}{*}{$\\left(\\pcbbd^{[LU]}\\right)^{-1}\\ac$} & $\\lambda_{\\min}$');
    for i = 1:numlev
        fprintf(fidApp,'& %4.2g ', eigmat(i,11));
    end
    fprintf(fidApp,'\\\\\n');
    
    fprintf(fidApp,'& $\\lambda_{\\max}$ ');
    for i = 1:numlev
        fprintf(fidApp,'& %4.3g ', eigmat(i,12));
    end
    fprintf(fidApp,'\\\\\n');
    
    fprintf(fidApp,'$\\multirow{2}{*}{$\\left(\\pcbbd^{[AMG]}\\right)^{-1}\\ac$} & $\\lambda_{\\min}$');
    for i = 1:numlev
        fprintf(fidApp,'& %4.2g ', eigmat(i,13));
    end
    fprintf(fidApp,'\\\\\n');
    
    fprintf(fidApp,'& $\\lambda_{\\max}$ ');
    for i = 1:numlev
        fprintf(fidApp,'& %4.3g ', eigmat(i,14));
    end
    fprintf(fidApp,'\\\\\n');
    
    
    
    % fid Other
    fprintf(fidOth,'$\\multirow{2}{*}{$\\pc_{AMG}^{-1}\\ac $} & $\\lambda_{\\min}$ ');
    for i = 1:numlev
        fprintf(fidOth,'& %g ', eigmat(i,3));
    end
    fprintf(fidOth,'\\\\\n');
    
    fprintf(fidOth,'$ & \\lambda_{\\max} ');
    for i = 1:numlev
        fprintf(fidOth,'& %4.3g ', eigmat(i,4));
    end
    fprintf(fidOth,'\\\\\n');
    
    fprintf(fidOth,'$\\mulitrow{2}{*}{$\\pc_{J}^{-1}\\ac)$ } & $\\lambda_{\\min}$');
    for i = 1:numlev
        fprintf(fidOth,'& %g ', eigmat(i,5));
    end
    fprintf(fidOth,'\\\\\n');
    
    fprintf(fidOth,'& $\\lambda_{\\max}$ ');
    for i = 1:numlev
        fprintf(fidOth,'& %4.3g ', eigmat(i,6));
    end
    fprintf(fidOth,'\\\\\n');
    
    fclose all;
end
