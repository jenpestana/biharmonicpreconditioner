% Computes effective condition number for each level

test_type = 1;    % 1: square, 2: stretched, 3: distorted, 4: curved
levset = 2:5;
numlev = length(levset);
effcond = zeros(2,numlev);
nxmat = zeros(2,numlev);
nbmat = zeros(2,numlev);

for levind = 1:numlev
    level = levset(levind);
    display(level)
 
     % Get A
    switch test_type 
        case 1
            stretch_a = '1.0';
            stretch_b = '1.0';
            domain_type = 'polygonal';
        case 2
            stretch_a = '2.5';
            stretch_b = '1.0';
            domain_type = 'polygonal';
        case 3
            stretch_a = '1.0';
            stretch_b = '1.5';
            domain_type = 'polygonal';
        case 4
            stretch_a = '1.0';
            stretch_b = '1.0';
            domain_type = 'curved';
    end
    bih_mat_rhs;
    
    n = size(A11,1);

    A = (A+A')/2; 
    eigopts.issym = 1;
    eigopts.maxit = 2000;
    eigopts.p = min(n,100);
    emin = eigs(A,1,'sa',eigopts);
    
    rng('default')
    b1 = randn(4*n,1); b1 = b1/norm(b1);    % random
    b2 = b;                                 % f=1
    x1 = A\b1;
    x2 = A\b2;
    eff1 = norm(b1)/(norm(x1)*emin);
    eff2 = norm(b2)/(norm(x2)*emin);
    nxmat(:,levind) = [norm(x1);norm(x2)];
    nbmat(:,levind) = [norm(b1); norm(b2)];
    
    effcond(:,levind) = [eff1; eff2];
end