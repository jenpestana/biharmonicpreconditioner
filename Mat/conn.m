function L = conn(lev)

% Produces connectivity matrix L
%
% 
asize = lev*2 - 2;
L = sparse(16*lev^2,asize^2);

% Element 1
ind = 16*0;
L(ind + 13:ind + 16, 1:4) = speye(4);

% Middle elements
for j = 1:lev-2
   ind = 16*j;
   st = 4*(j-1)+1;
   L(ind + 9:ind + 16,st:st+7) = speye(8);
end

% End element
ind = 16*(lev-1);
st = 4*(lev-2)+1;
L(ind + 9:ind + 12, st:st+3) = speye(4);


% Middle rows
for k = 1:lev-2
    ind = 16*lev*k;
    rst1 = (lev-1)*4*(k-1)+1;
    rst2 = (lev-1)*4 + (lev-1)*4*(k-1)+1;
    % Element 1 in row
    L(ind + 5:ind + 8, rst1:rst1+3) = speye(4);
    L(ind + 13:ind + 16, rst2:rst2+3) = speye(4);
    
    % Elements 2:15 in row
    for j = 1:lev-2
        ind = 16*(k*lev+j);
        st1 = rst1 + 4*(j-1);
        st2 = rst2 + 4*(j-1);
        L(ind + 1:ind + 8,st1:st1+7) = speye(8);
        L(ind + 9:ind + 16, st2:st2+7) = speye(8);
    end
    
    % Last element in row
    ind = 16*((k+1)*lev-1);
    st1 = rst1 + 4*(lev-2);
    st2 = rst2 + 4*(lev-2);
    L(ind + 1:ind + 4,st1:st1+3) = speye(4);
    L(ind + 9:ind + 12, st2:st2+3) = speye(4);
end

% Last row, first element
ind = 16*lev*(lev-1);
rst = (lev-1)*4*(lev-2)+1;
L(ind+5:ind+8,rst:rst+3) = speye(4);

% Elements 2:15 in row
for j = 1:lev-2
        ind = 16*(lev*(lev-1)+j);
        st1 = rst+4*(j-1);
        L(ind + 1:ind + 8,st1:st1+7) = speye(8);
end

% Last element in row
ind = 16*(lev^2-1);
st = rst + 4*(lev-2);
L(ind+1:ind+4,st:st+3) = speye(4);